let bankAccount = 0, payAmount = 0, loanAmount = 0;
let balanceText = document.getElementById("balance");
let payText = document.getElementById("payAmount");
let loanText = document.getElementById("loanAmount");
balanceText.innerText = bankAccount;
payText.innerText = payAmount;
loanText.innerText = loanAmount;
document.getElementById("loanDiv").style.display = "none";
let workBtn = document.getElementById("workBtn");
let bankBtn = document.getElementById("bankBtn");
let loanBtn = document.getElementById("loanBtn");
let payLoanBtn = document.getElementById("payLoanBtn");
let selectBox = document.getElementById("laptopSelect");
let featureList = document.getElementById("featureList");
let buyBtn = document.getElementById("buyBtn");
buyBtn.addEventListener("click", handleBuy);
workBtn.addEventListener('click',handleWork);
bankBtn.addEventListener("click",handlePay);
loanBtn.addEventListener("click",handleLoan);
payLoanBtn.addEventListener("click", handlePayback)
selectBox.addEventListener("change", displayFeatures)
let laptop;
let apiData = null;
getApi()

// Function that runs when the user tries to buy a laptop. 
// Laptop will either get bought and the cost will be subtracted from
// the balance or the user will get a message that they can't buy the laptop
function handleBuy() {
    if (bankAccount < apiData[laptop-1].price){
        alert("You don't have enough money for this laptop");
    } else {
        bankAccount -= apiData[laptop-1].price;
        balanceText.innerText = bankAccount;
        alert(`You are now the owner of ${apiData[laptop-1].title}!`);
    }

}

// fetches the api and displays the initial data in the application.
async function getApi() {
    await fetch("https://hickory-quilled-actress.glitch.me/computers")
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    apiData = data;
                })
                .then(displayComputers)
}     

// Gives the user money when the work button is pressed. 
function handleWork() {
    payAmount += 100;
    payText.innerText = payAmount;
}

// displays data from the api based on which laptop is chosen in the laptop
// select field
function displayFeatures(){
    laptop = document.getElementById("laptopSelect").value;
    let list = "<li>" + apiData[laptop-1].specs.join("</li><li>") + "</li";
    document.getElementById("featureList").innerHTML = list;
    document.getElementById("infoTitle").innerHTML = apiData[laptop-1].title;
    document.getElementById("infoDesc").innerHTML = apiData[laptop-1].description;
    document.getElementById("price").innerHTML = apiData[laptop-1].price;
    document.getElementById("laptopImage").src = `https://hickory-quilled-actress.glitch.me/${apiData[laptop-1].image}`;
}

// fills the select field with laptops from the api.
function displayComputers() {
    let list = "";
    apiData.forEach(laptop => {
        list += `<option value="${laptop.id}">${laptop.title}</option>`
    });
    document.getElementById("laptopSelect").innerHTML = list;
    displayFeatures();

}


// Handles money correct when the user tries to pay back loan.
function handlePayback() {
    if (payAmount < loanAmount){
        loanAmount -= payAmount;
        payAmount = 0;
        payText.innerText = payAmount; 
        loanText.innerText = loanAmount;   
    }  else {
        payAmount -= loanAmount;
        loanAmount = 0;
        payText.innerText = payAmount;
        document.getElementById("loanDiv").style.display = "none";
    }
}

//Handles money right when the user tries to bank their money.
function handlePay() {
    if (loanAmount == 0) {
        bankAccount += payAmount;
        payAmount = 0;
        balanceText.innerText = bankAccount;
        payText.innerText = payAmount;    
    } else {
        bankAccount += payAmount*0.9;
        loanAmount -= payAmount*0.1;
        payAmount = 0;
        balanceText.innerText = bankAccount;
        payText.innerText = payAmount;
        if (loanAmount <= 0){
            loanAmount = 0;
            document.getElementById("loanDiv").style.display = "none";
        } else {
            loanText.innerText = loanAmount;    
        }
    }
   
}

// Handles loan functionality when the user tries to take one. 
function handleLoan() {
    console.log(loanAmount)
    if (loanAmount != 0){
        alert("You need to pay back your current loan first!");
        return;
    } else {
        let loanPrompt = window.prompt('How much loan do you need?');
        if (isNaN(loanPrompt) || loanPrompt < 1){
            alert("You need to enter a number higer than 0");
            return;
        } else if (loanPrompt > (bankAccount*2)){
            alert("You can't loan more than double your bank account!");
        } else {
            loanAmount = loanPrompt;
            loanText.innerText = loanAmount;
            bankAccount += Number(loanAmount)   ;
            console.log(bankAccount)
            balanceText.innerText = bankAccount;
            document.getElementById("loanDiv").style.display = "initial";
        }

    }
     
}